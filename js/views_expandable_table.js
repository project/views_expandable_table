(function (Drupal, $, once) {
  Drupal.behaviors.viewsExpandableTable = {
    attach(context, settings) {

      if (settings.views_expandable_table.triggerable_row) {
        const $elements = $(once('viewsExpandableTable','tr[data-views-expandable-table-trigger]', context));

        $elements.each(function () {
          var $trigger = $(this);
          var table = $trigger.parents('table')[0];
          var $target = $('tr[data-views-expandable-table-target="' + this.dataset.viewsExpandableTableTrigger + '"]', table);

          // Toggle when trigger clicked.
          $trigger.click(function () {
            toggleExpanded($trigger, $target, settings, tr = undefined);
          });
          // Allow some elements within trigger element to be clicked w/o toggle.
          $('a, btn, input', $trigger).click(function (event) {
            event.stopPropagation();
          });

          // Allow hover to apply to both rows.
          $trigger.hover(function () {
            toggleHover($trigger, $target);
          }, function () {
            toggleHover($trigger, $target);
          });
          $target.hover(function () {
            toggleHover($trigger, $target);
          }, function () {
            toggleHover($trigger, $target);
          });
        });
      }
      else {
        const $elements = $(once('viewsExpandableTable','span.views-expandable-table-trigger', context));
        $elements.each(function () {
          var $trigger = $(this);
          var table = $trigger.parents('table')[0];
          var td = $trigger.parents('td')[0];
          var $tr = $($trigger.parents('tr')[0]);
          var $target = $('tr[data-views-expandable-table-target="' + td.dataset.viewsExpandableTableTarget + '"]');

          $trigger.click(function () {
            toggleExpanded($trigger, $target, settings, $tr);
          });
          // Allow some elements within trigger element to be clicked w/o toggle.
          $('a, btn, input', $trigger).click(function (event) {
            event.stopPropagation();
          });

          // Allow hover to apply to both rows.
          $trigger.hover(function () {
            toggleHover($trigger, $target);
          }, function () {
            toggleHover($trigger, $target);
          });
          $target.hover(function () {
            toggleHover($trigger, $target);
          }, function () {
            toggleHover($trigger, $target);
          });
        });
      }
    }
  };

  // Detects if either element is being hovered.
  function isHover($trigger, $target) {
    return $trigger.is(":hover") || $target.is(":hover");
  }

  // Toggle the expanded class on both elements, based on status of trigger.
  function toggleExpanded($trigger, $target, settings, $tr) {
    if (settings.views_expandable_table.triggerable_row != undefined) {
      $trigger.toggleClass('expanded');
      $target.toggleClass('expanded', $trigger.hasClass('expanded'));
    }
    else {
      $trigger.toggleClass('expanded');
      $tr.toggleClass('views-expandable-table-trigger expanded');
      $target.toggleClass('expanded', $tr.hasClass('expanded'));
    }
  }

  // Toggle hover class on hover for either element.
  function toggleHover($trigger, $target, settings, $tr) {
    var hover = isHover($trigger, $target);
    $trigger.toggleClass('views-expandable-table-hover', hover);
    $target.toggleClass('views-expandable-table-hover', hover);
  }
}(Drupal, jQuery, once));
