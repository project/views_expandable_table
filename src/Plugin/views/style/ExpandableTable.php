<?php

namespace Drupal\views_expandable_table\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\Table;

/**
 * Style plugin to render each item as a row in an expandable table.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_view_expandabletable",
 *   title = @Translation("Expandable Table"),
 *   help = @Translation("Displays rows in an expandable table."),
 *   theme = "views_view_expandabletable",
 *   display_types = {"normal"}
 * )
 */
class ExpandableTable extends Table {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['triggerable_row'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['triggerable_row'] = [
      '#type' => 'checkbox',
      '#title' => 'Use triggerable row',
      '#default_value' => $this->options['triggerable_row'],
      '#description' => $this->t('If this is checked, the entire row will be triggered. If not, a special element will be added to the end of the last visible @td element.', ['@td' => '<td>']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['#attached']['library'][] = 'views_expandable_table/expandable_table';
    $build['#attached']['drupalSettings']['views_expandable_table']['triggerable_row'] = $this->options['triggerable_row'];
    return $build;
  }

}
